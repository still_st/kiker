//Popup-forms
//advantages
jQuery(document).on('click', '#popup-advantages__close', function()
{
    jQuery('#popup-advantages').slideToggle();
});

jQuery(document).on('click', '.popup-advantages__open', function()
{
    jQuery('#popup-advantages').slideToggle();
});

jQuery(document).on('click', '#popup-advantages', function()
{
   if (jQuery('#popup-advantages .popup__body:hover').length == 0){
       jQuery("#popup-advantages").slideToggle();
   }
});


//buy
jQuery(document).on('click', '#popup-buy__close', function()
{
    jQuery('#popup-buy').slideToggle();
});

jQuery(document).on('click', '#popup-buy__open', function()
{
    jQuery('#popup-buy').css("display", "block");
});

jQuery(document).on('click', '#popup-buy', function()
{
   if (jQuery('#popup-buy .popup__body:hover').length == 0){
       jQuery("#popup-buy").slideToggle();
   }
});

//tournament
jQuery(document).on('click', '#popup-tournament__close', function()
{
    jQuery('#popup-tournament').slideToggle();
});

jQuery(document).on('click', '#popup-tournament__open', function()
{
    jQuery('#popup-tournament').css("display", "block");
});

jQuery(document).on('click', '#popup-tournament', function()
{
   if (jQuery('#popup-tournament .popup__body:hover').length == 0){
       jQuery("#popup-tournament").slideToggle();
   }
});


//slider
jQuery(document).on('click', '#popup-slider__close', function()
{
    jQuery('#popup-slider').slideToggle();
});

jQuery(document).on('click', '#popup-slider__open', function()
{
    jQuery('#popup-slider').css("display", "block");
});

jQuery(document).on('click', '#popup-slider', function()
{
   if (jQuery('#popup-slider .popup__body:hover').length == 0){
       jQuery("#popup-slider").slideToggle();
   }
});


//thanks
jQuery(document).on('click', '#popup-thanks__close', function()
{
    jQuery('#popup-thanks').slideToggle();
});

jQuery(document).on('click', '#popup-thanks__open', function()
{
    jQuery('#popup-thanks').css("display", "block");
});

jQuery(document).on('click', '#popup-thanks', function()
{
   if (jQuery('#popup-thanks .popup__body:hover').length == 0){
       jQuery("#popup-thanks").slideToggle();
   }
});


//SCROLL
smoothScroll.init(
{
    selector: '[data-scroll]', // Selector for links (must be a class, ID, data attribute, or element tag)
    selectorHeader: null, // Selector for fixed headers (must be a valid CSS selector) [optional]
    speed: 800, // Integer. How fast to complete the scroll in milliseconds
    easing: 'easeInOutCubic', // Easing pattern to use
    offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
}

  );

//CAROUSEL
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    navText:["",""],
    items:1
})


//MAP
/*
AIzaSyB3407DTDq4AIf0msmaYqC7AYmRjNIXIkI
*/
google.maps.event.addDomListener(window, 'load', init);

function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 14,
        scrollwheel: false,
        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(59.978097, 30.283482), //

        // How you would like to style the map.
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
    };

    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('google-map');

    // Create the Google Map using our element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);

    // Let's also add a marker while we're at it
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(59.978313, 30.314262),
        map: map,
        title: 'Snazzy!'
    });
}




function popupFormSendOk(){
  var element = document.querySelector("#popup-advantages .popup__body .popup__title");
  element.parentNode.removeChild(element);
  element = document.querySelector("#popup-advantages .popup__body .popup__txt");
  element.parentNode.removeChild(element);
  element = document.querySelector("#popup-advantages .popup__body form");
  element.parentNode.removeChild(element);
  var p = document.createElement("p");
  var thanks = document.createTextNode("Спасибо!");
  p.appendChild(thanks);
  p.classList.add("thanks");
  element = document.querySelector("#popup-advantages .popup__body");
  element.appendChild(p);
  p = document.createElement("p");
  var weCallYou = document.createTextNode("В ближайшее время мы свяжемся с вами");
  p.appendChild(weCallYou);
  p.classList.add("wecallyou");
  element = document.querySelector("#popup-advantages .popup__body");
  element.appendChild(p);
}

function popupFormBuySendOk(){
  var element = document.querySelector("#popup-buy .popup__body .popup__title");
  element.parentNode.removeChild(element);
  element = document.querySelector("#popup-buy .popup__body .popup__txt");
  element.parentNode.removeChild(element);
  element = document.querySelector("#popup-buy .popup__body form");
  element.parentNode.removeChild(element);
  var p = document.createElement("p");
  var thanks = document.createTextNode("Спасибо!");
  p.appendChild(thanks);
  p.classList.add("thanks");
  element = document.querySelector("#popup-buy .popup__body");
  element.appendChild(p);
  p = document.createElement("p");
  var weCallYou = document.createTextNode("В ближайшее время мы свяжемся с вами");
  p.appendChild(weCallYou);
  p.classList.add("wecallyou");
  element = document.querySelector("#popup-buy .popup__body");
  element.appendChild(p);
}

function popupFormTournamentSendOk(){
  var element = document.querySelector("#popup-tournament .popup__body .popup__title");
  element.parentNode.removeChild(element);
  element = document.querySelector("#popup-tournament .popup__body .popup__txt");
  element.parentNode.removeChild(element);
  element = document.querySelector("#popup-tournament .popup__body .tournament-list");
  element.parentNode.removeChild(element);
  element = document.querySelector("#popup-tournament .popup__body form");
  element.parentNode.removeChild(element);

  var p = document.createElement("p");
  var thanks = document.createTextNode("Спасибо!");
  p.appendChild(thanks);
  p.classList.add("thanks");
  element = document.querySelector("#popup-tournament .popup__body");
  element.appendChild(p);
  p = document.createElement("p");
  var weCallYou = document.createTextNode("В ближайшее время мы свяжемся с вами");
  p.appendChild(weCallYou);
  p.classList.add("wecallyou");
  element = document.querySelector("#popup-tournament .popup__body");
  element.appendChild(p);
}