<div class="popup" id="popup-tournament">
    <div class="popup__body popup__body--tournament">
      <div class="popup__close" id="popup-tournament__close">

      </div>
      <p class="popup__title">
         Турнир «под ключ»
      </p>
      <p class="popup__txt">
        Мы готовы и предлагаем организовать для вас грандииозное спортивно-развлекательное событие, с регистрацией команд и настоящим судейством. Команды будут состязаться за титул чемпионов компании, заведения или района. Атмосферу Чемпионата мира по футболу вам подарит наша профессиональная команда - ведущий, DJ, судейская коллегия, специалисты по оформлению и другие. Мы знаем как важны детали, и будем обсуждать с вами все идеи для того, чтобы понимать, что мы на «одной волне».
      </p>
      <ul class="tournament-list">
        <li>Выбор площадки</li>
        <li>Ведущий</li>
        <li>Инструкторы</li>
        <li>Брендирование</li>
        <li>Концепция</li>
        <li>Кейтеринг</li>
        <li>Фотограф</li>
        <li>PR и реклама</li>
        <li>Сценарий</li>
        <li>Призы, пьедестал</li>
        <li>Музыка, Dj</li>
        <li>Оформление</li>
      </ul>
      <form action="#">
            <span>
              <input type="text" aria-invalid="false" placeholder="Ваше имя">
            </span>
            <span>
              <input type="tel" placeholder="Телефон" required="required">
            </span>
            <button class="btn">
                Отправить
            </button>
      </form>
    </div>
</div>