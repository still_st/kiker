<header>
    <div class="wrapper">

        <a href="/" class="logo">
            <img src="img/logo.svg" alt="Логотип Derbi">
        </a>
        <nav>
            <ul>
                <li>
                    <a href="#about" data-scroll>о нас</a>
                </li>
                <li>
                    <a href="#offer" data-scroll>услуги</a>
                </li>
                <li>
                    <a href="#google-map" data-scroll>контакты</a>
                </li>
            </ul>
        </nav>
        <div class="order">
            <a class="order__tel" href="tel:+79531542193">
                +7 (953) 154-21-93
            </a>
            <a class="btn popup-advantages__open">
                обратный звонок
            </a>
        </div>
    </div>
</header>