<div class="popup" id="popup-buy">
    <div class="popup__body">
      <div class="popup__close" id="popup-buy__close">

      </div>
      <p class="popup__title">
       Хотите купить стол?
      </p>
      <p class="popup__txt">
        Оставьте свои контакты и мы перезвоним вам, чтобы рассказать подробности!
      </p>
      <form action="#">
            <span>
              <input type="text" aria-invalid="false" placeholder="Ваше имя">
            </span>
            <span>
              <input type="tel" placeholder="Телефон" required="required">
            </span>
            <button class="btn">
                Отправить
            </button>
      </form>
    </div>
</div>