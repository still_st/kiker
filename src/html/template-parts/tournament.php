<section id="tournament" class="tournament">
    <div class="wrapper">
        <h2 class="title title--tournament">Турнир «под ключ»</h2>
        <p class="tournament__txt ">
            С удовольствием возьмём на себя организацию турнира по настольному футболу (кикеру).
            Для этого у нас есть все необходимые ресурсы и опыт, который поможет предложить вам
            оптимальный вариант мероприятия исходя из вашего бюджета.
        </p>
        <a class="btn" id="popup-tournament__open">Подробнее</a>
    </div>
</section>