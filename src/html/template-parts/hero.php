<div class="hero">
    <div class="wrapper">
        <h1 class="hero__title">Аренда кикера (настольный футбол) в Санкт-Петербурге</h1>
        <p class="hero__legend">
            Для корпоративов, презентаций, дней рождений,
            свадеб, мальчишников, и других мероприятий
        </p>
        <p class="hero__order-txt">
            Узнайте подробнее
        </p>
        <p class="hero__order-txt">
            о нашем предложении
        </p>
        <div class="tac">
            <a class="btn-trans" href="#offer" data-scroll>Перейти к услугам</a>
        </div>
    </div>
</div>