<section class="about" id="about">
    <div class="wrapper">
        <div class="line">
            <div class="col-6 col-m-12 col-s-12">
                <h2 class="title title--about">О нас</h2>
                <p class="txt">
                    Компания "Derbi" - российский производитель столов для игры в настольный футбол (кикер).
                    Мы предлагаем использовать наши столы в аренду для организации игровой зоны на различных мероприятиях.
                </p>
            </div>
            <div class="col-6 col-m-12 col-s-12">
                <div class="about__img">
                    <img src="img/table.png" alt="Кикер">
                </div>
            </div>
        </div>
    </div>
</section>