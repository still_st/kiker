<div class="popup" id="popup-advantages">
    <div class="popup__body">
      <div class="popup__close" id="popup-advantages__close">

      </div>
      <p class="popup__title">
        Оставить заявку
      </p>
      <p class="popup__txt">
        Укажите ваше имя и номер телефона, чтобы мы могли связаться с вами!
      </p>
      <form action="#">
            <span>
              <input type="text" aria-invalid="false" placeholder="Ваше имя">
            </span>
            <span>
              <input type="tel" placeholder="Телефон" required="required">
            </span>
            <button class="btn">
                Отправить
            </button>
      </form>
    </div>
</div>