<div class="popup" id="popup-thanks">
    <div class="popup__body popup__body--thanks">
      <div class="popup__close" id="popup-thanks__close">

      </div>
      <p class="popup__title">
        Спасибо!
      </p>
      <p class="popup__txt popup__txt--thanks">
        В ближайшее время мы свяжемся с вами.
      </p>

    </div>
</div>