            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3407DTDq4AIf0msmaYqC7AYmRjNIXIkI"></script>

            <!-- JS libraries -->
                  <!-- build:js js/vendor.js -->
                        <!-- bower:js -->
                              <script src="bower/jquery/dist/jquery.min.js"></script>
                              <script src="bower/smooth-scroll/dist/js/smooth-scroll.min.js"></script>
                              <script src="bower/owl.carousel/dist/owl.carousel.min.js"></script>
                        <!-- endbower -->
                  <!-- endbuild -->
            <!-- build:js js/script.js -->
                  <script src="js/jquery.scrollme.min.js"></script>
                  <script src="js/script.js"></script>
            <!-- endbuild -->
    </body>
</html>