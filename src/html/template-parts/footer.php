<footer class="footer">
    <div class="wrapper">
        <div class="logo">
            <img src="img/logo.svg" alt="Логотип Derbi">
        </div>
        <ul class="links">
            <li><a href="https://vk.com/kiker_derbi"><img src="img/vk.svg" alt="vk"></a></li>
            <li><a href="https://twitter.com/kiker_derbi"><img src="img/twitter.svg" alt="twitter"></a></li>
            <li><a href="https://www.instagram.com/kicker_derbi/"><img src="img/instagram.svg" alt="instagram"></a></li>
            <li><a href="https://vk.com/kiker_derbi"><img src="img/youtube.svg" alt="youtube"></a></li>
        </ul>
        <div class="btn-wrap">
            <a class="btn-green btn-green--footer" id="popup-buy__open">Купить стол</a>
        </div>
    </div>
</footer>