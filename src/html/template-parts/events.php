<section class="events">
      <div class="tuti-wrapper">
            <img class="events__img" src="img/lesha.png" alt="Derbi Алексей">
            <div class="tuti-wrapper__content">
                 <h2 class="title title--events">Мероприятия</h2>
                      <p class="txt txt--events">
                          За два года работы, наша компания успела
                          поучаствовать уже в более 50 мероприятиях
                          различного уровня, от небольших площадок до
                          масштабных фестивалей
                      </p>
                      <a class="btn" id="popup-slider__open">Смотреть фото</a>
                  </div>
            </div>
      </div>
</section>