<div class="price">
    <div class="wrapper">
        <ul class="line">
            <li class="col-4 col-m-6 col-s-12">
                <div class="price__item">
                    <div class="price__item-title">
                        Пакет «Стандартный»
                    </div>
                    <ul>
                        <li>
                            Аренда стола(ов)
                        </li>
                        <li>
                            Доставка, разгрузка, монтаж/демонтаж
                        </li>
                    </ul>
                    <div class="price__item-price">
                        от 6000 руб.
                    </div>
                </div>
                <a class="btn popup-advantages__open">Узнать подробнее</a>
            </li>
            <li class="col-4 col-m-6 col-s-12">
                <div class="price__item">
                    <div class="price__item-title">
                        Пакет «Развлекательный»
                    </div>
                    <ul>
                        <li>
                            Аренда стола(ов)
                        </li>
                        <li>
                            Доставка, разгрузка, монтаж/демонтаж
                        </li>
                        <li>
                            Работа инструктора(ов)
                        </li>
                    </ul>
                    <div class="price__item-price">
                        от 7500 руб.
                    </div>
                </div>
                <a class="btn popup-advantages__open">Узнать подробнее</a>
            </li>
            <li class="col-4 col-m-6 col-s-12">
                <div class="price__item">
                    <div class="price__item-title">
                        Пакет «Улётный»
                    </div>
                    <ul>
                        <li>
                            Аренда стола(ов)
                        </li>
                        <li>
                            Доставка, разгрузка, монтаж/демонтаж
                        </li>
                        <li>
                            Работа инструктора(ов)
                        </li>
                        <li>
                            Проведение турнира
                        </li>
                        <li>
                            Призы, пьедестал
                        </li>
                        <li>
                            Фоторепортаж
                        </li>
                    </ul>
                    <div class="price__item-price">
                        от 12000 руб.
                    </div>
                </div>
                <a class="btn popup-advantages__open">Узнать подробнее</a>
            </li>
        </ul>
    </div>
</div>
