 <div class="advantages">
    <div class="wrapper">
        <div class="right-block">
            <ul>
                <li>
                    <span class="advantages__title">Индивидуальный подход</span>
                    <p class="advantages__txt">
                        Мы всегда спрашиваем о формате вашей аудитории чтобы предложить соответсвующее решение
                    </p>
                </li>
                <li>
                    <span class="advantages__title">Все свое</span>
                    <p class="advantages__txt">
                        Чтобы не допустить «осечки», у нас всегда есть под рукой необходимые инструменты и детали
                    </p>
                </li>
                <li>
                    <span class="advantages__title">Доверяем вам</span>
                    <p class="advantages__txt">
                        Работаем без залога, поэтому вам не нужно волноваться, где взять дополнительные средства
                    </p>
                </li>
            </ul>
            <a class="btn-trans btn-trans--advantages popup-advantages__open">Оформить заказ</a>
        </div>
    </div>
</div>