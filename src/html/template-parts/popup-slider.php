<div class="popup" id="popup-slider">
    <div class="popup__body popup__body--slider">
      <div class="popup__close popup__close--slider" id="popup-slider__close">

      </div>
      <div class="slider">
          <ul class="owl-carousel">
                <li>
                      <div class="slider__img">
                          <img src="img/slider/vk.jpg" title="Кикер фестиваль Вконтакте" alt="Кикер фестиваль в контакте">
                      </div>
                      <p class="slider__caption">
                          Фестиваль Вконтакте
                      </p>
                </li>
                <li>
                      <div class="slider__img">
                          <img src="img/table.png" title="Кикер фестиваль Вконтакте" alt="Кикер фестиваль в контакте">
                      </div>
                      <p class="slider__caption">
                          Фестиваль в контакте
                      </p>
                </li>
                <li>
                      <div class="slider__img">
                          <img src="img/slider/vk.jpg" title="Кикер фестиваль Вконтакте" alt="Кикер фестиваль в контакте">
                      </div>
                      <p class="slider__caption">
                          Фестиваль в контакте
                      </p>
                </li>
                <li>
                      <div class="slider__img">
                          <img src="img/table.png" title="Кикер фестиваль Вконтакте" alt="Кикер фестиваль в контакте">
                      </div>
                      <p class="slider__caption">
                          Фестиваль в контакте
                      </p>
                </li>
                <li>
                      <div class="slider__img">
                          <img src="img/slider/vk.jpg" title="Кикер фестиваль Вконтакте" alt="Кикер фестиваль в контакте">
                      </div>
                      <p class="slider__caption">
                          Фестиваль в контакте
                      </p>
                </li>
                <li>
                      <div class="slider__img">
                          <img src="img/table.png" title="Кикер фестиваль Вконтакте" alt="Кикер фестиваль в контакте">
                      </div>
                      <p class="slider__caption">
                          Фестиваль в контакте
                      </p>
                </li>
          </ul>
      </div>
    </div>
</div>