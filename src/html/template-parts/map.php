<section class="map">
    <div id="google-map">

    </div>
    <div class="wrapper">
        <div class="map__contacts">
            <h2 class="title title--map">Контакты</h2>
            <span>Адрес:</span>
            <p>Аптекарская набережная, д.20 лит.А</p>
            <span>Телефон:</span>
            <p>+7(953) 154-21-31, +7(952) 382-95-75</p>
            <span>Электронная почта</span>
            <p>info@arendakikerspb.ru</p>
        </div>
    </div>
</section>
